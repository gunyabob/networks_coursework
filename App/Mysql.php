<?

class Mysql
{    
    public static function get()
    {
        require $_SERVER['DOCUMENT_ROOT'].'/config.php';
        $mysqli = new mysqli($db_host,$db_user,$db_pass,$db_name);
        if ($mysqli->connect_errno) {
            echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
        }
        return $mysqli;
    }
    public static function close($mysql) {
        $mysql->close();
    }
}

?>