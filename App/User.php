<?

class User
{
    public $type='';
    public $id='';
    public $token='';
    public $name='';
    
    function __construct($token = null)
    {
        if (empty($token)) {
            $this->type = "anonym";
            return;
        }
        $this->findUser($token);
    }
    function findUser($token) {
        $m = Mysql::get();
        $stmt = $m->prepare("SELECT id, name, type, token FROM users WHERE token=?");
        $stmt->bind_param('s', $token);
        $stmt->execute();
        $res = $stmt->get_result();
        if ($res->num_rows > 0) {
            $r = $res->fetch_assoc();
            $this->id = $r['id'];
            $this->name = $r['name'];
            $this->type = $r['type'];
            $this->token = $r['token'];
        } else {
            $this->type = "anonym";
        }
        $stmt->close();
        Mysql::close($m);
    }
    function login($login, $pass) {
        $m = Mysql::get();
        $stmt = $m->prepare("SELECT id, name, type, token FROM users WHERE name=? and pass=MD5(CONCAT(MD5(?),salt))");
        $stmt->bind_param('ss', $login, $pass);
        $stmt->execute();
        $res = $stmt->get_result();
        if ($res->num_rows > 0) {
            $r = $res->fetch_assoc();
            $this->id = $r['id'];
            $this->name = $r['name'];
            $this->type = $r['type'];
            $this->token = $r['token'];
        } else {
            $this->type = "anonym";
        }
        $stmt->close();
        Mysql::close($m);
    }
    function register($login, $pass, $age, $male) {
        $m = Mysql::get();
        if (strlen($login)<1 || strlen($pass)<1
            || $age<1 
            || ($male != 'male' && $male != 'female')) {
            $this->type = "anonym";
            return;
        }
        $salt = Utils::randomString();
        $token = Utils::randomString();
        $stmt = $m->prepare("INSERT INTO users (name, salt, pass, age, sex, token) VALUES(?,?,MD5(CONCAT(MD5(?),salt)),?,?,?)");
        if (!$stmt) {
            return;
        }
        $stmt->bind_param('sssiss', $login, $salt, $pass, $age, $male, $token);
        
        $stmt->execute();
        if ($stmt->affected_rows > 0) {
            $this->id = $m->insert_id;
            $this->name = $login;
            $this->type = 'user';
            $this->token = $token;
        } else {
            $this->type = "anonym";
        }
        $stmt->close();
        Mysql::close($m);
    }
}

?>